import java.util.concurrent.locks.*;

public class BankAccount
{
    private double balance;
    private Lock balanceChangeLock;
    private Condition notNegative;
    
    public BankAccount()
    {
	balanceChangeLock = new ReentrantLock();
	notNegative = balanceChangeLock.newCondition();
        balance = 0;
    }

    public void deposit(double amount)
    {
	balanceChangeLock.lock();
        System.out.print("Depositing " + amount);
        double newBalance = balance + amount;
        System.out.println(", new balance is " + newBalance);
        balance = newBalance;
	notNegative.signalAll();
	
	balanceChangeLock.unlock();


    }

    public void withdraw(double amount)
    {
	balanceChangeLock.lock();
	try{

	    while (balance < amount)
		notNegative.await();
	    
        System.out.print("Withdrawing " + amount);
        double newBalance = balance - amount;
        System.out.println(", new balance is " + newBalance);
        balance = newBalance;
	} catch(InterruptedException ex){}
	finally{balanceChangeLock.unlock();}

    }

    public double getBalance()
    {
        return balance;
    }
}
